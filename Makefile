.PHONY: test setup
PROJECT_NAME=tf-redirect-s3
VERSION=0.1.0
TARGET_DIR=target
ARTIFACT_DIR=$(TARGET_DIR)/$(PROJECT_NAME)/$(VERSION)/artifacts
REPORT_DIR=$(ARTIFACT_DIR)/reports

KITCHEN=kitchen
KITCHEN_OPTIONS=
INSPEC=inspec
INSPEC_ATTRIBUTES=./test/fixtures/example1/attributes.yml
INSPEC_REPORTS=cli json:$(REPORT_DIR)/inspec.json html:$(REPORT_DIR)/inspec.html
INSPEC_OPTIONS=-t aws://us-east-1 --reporter $(INSPEC_REPORTS) --attrs $(INSPEC_ATTRIBUTES)

setup:
	@$(KITCHEN) $(KITCHEN_OPTIONS) destroy
	@$(KITCHEN) $(KITCHEN_OPTIONS) create
	@$(KITCHEN) $(KITCHEN_OPTIONS) converge
	@$(KITCHEN) $(KITCHEN_OPTIONS) setup

test: setup
	@mkdir -p $(REPORT_DIR)
	@$(INSPEC) exec test/integration/example1 $(INSPEC_OPTIONS) 
	@$(KITCHEN) $(KITCHEN_OPTIONS) destroy
