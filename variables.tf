variable "zone_name" {
  type = "string"
}

variable "redirect_protocol" {
  type = "string"
  default = "http"
}

variable "redirect_host" {
  type = "string"
}

variable "redirect_path" {
  type = "string"
  default = "/"
}

variable "bucket_prefix" {
  type = "string"
  default = "redirect"
}

