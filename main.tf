resource "aws_s3_bucket" "current" {
  bucket = "${var.bucket_prefix}.${var.zone_name}"
  acl = "public-read"
  provider = "aws"

  website {
    index_document = "index.html"

    routing_rules = <<EOF
[
  {
    "Redirect": {
      "HostName": "${var.redirect_host}",
      "Protocol": "${var.redirect_protocol}",
      "ReplaceKeyPrefixWith": "${var.redirect_path}",
      "HttpRedirectCode": "301"
    }
  }
]
EOF
  }

}

data "aws_route53_zone" "current" {
  name = "${var.zone_name}."
}

resource "aws_route53_record" "current" {
  zone_id = "${data.aws_route53_zone.current.zone_id}"
  name = "${var.bucket_prefix}"
  type = "A"

  alias {
    name = "${aws_s3_bucket.current.website_domain}"
    zone_id = "${aws_s3_bucket.current.hosted_zone_id}"
    evaluate_target_health = true
  }
}
 
