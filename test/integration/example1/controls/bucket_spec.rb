bucket_name = attribute('bucket_name')

control "bucket" do
  describe aws_s3_bucket(bucket_name: bucket_name) do
    it { should exist }
    it { should be_public }
  end
end

