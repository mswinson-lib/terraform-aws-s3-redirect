# tf-redirect-s3

[Terraform Module]() creates S3 redirect bucket

## Features


## Usage

```HCL
module "redirect" {
  source = "bitbucket.org/mswinson-lib/tf-redirect-s3"

  providers {                          # required
    aws = "<your provider>"
  }

  redirect_host = "<host>"             # required
  zone_name = "<domain zone name>"     # required

  redirect_protocol = "https"          # default
  redirect_path = "/"                  # default
  bucket_prefix = "redirect"           # default
}
```

## Inputs

| Name | Description | Type | Default | Required |
| - | - | - | - | - |
| redirect_host | | string | | true |
| zone_name | | string | | true |
| redirect_protocol | | string | "https" | true |
| redirect_path |  | string | "/" | true |
| bucket_prefix | | string | "redirect" | true |

## Outputs

  None
