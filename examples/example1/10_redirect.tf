module "example1" {
  source = "../../"

  zone_name = "dev.mswinson.com"
  providers = {
    aws = "aws.us-east"
  }

  bucket_prefix = "example1-redirect"
  redirect_host = "www.google.com"
}
